package com.example.matacpu;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.view.View;


public class MainActivity extends AppCompatActivity
{
    private EditText Entrada;
    private Button Suma;
    private Button Resta;
    private Button Multiplicacion;
    private Button Division;
    private Button Potenciacion;
    private Button Radicacion;
    private Button Exit;
    private TextView Salida;
    private EditText entrada;


    @Override
    protected void onCreate(Bundle savedInstanceState)
    {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        Entrada=findViewById(R.id.Entrada);
        entrada=findViewById(R.id.entrada);
        Suma=findViewById(R.id.Suma);
        Resta=findViewById(R.id.Resta);
        Multiplicacion=findViewById(R.id.Multiplicacion);
        Division=findViewById(R.id.Division);
        Potenciacion=findViewById(R.id.Potenciacion);
        Radicacion=findViewById(R.id.Radicacion);
        Exit=findViewById(R.id.Exit);
        Salida=findViewById(R.id.Salida);

        Suma.setOnClickListener(new View.OnClickListener()
        {
            @Override
            public void onClick(View v)
            {
                String Primera;
                String Segunda;
                Primera=Entrada.getText().toString();
                Segunda=entrada.getText().toString();
                if (Primera.equals("")||Segunda.equals(""))
                {
                    Salida.setText("Coloque un valor valido en las entradas");
                }
                else
                {
                    Double num1=Double.parseDouble(Primera);
                    Double num2=Double.parseDouble(Segunda);
                    Double num3= num1+num2;
                    final String b=String.valueOf(num3);
                    Salida.setText(b);
                }

            }
        });
        Resta.setOnClickListener(new View.OnClickListener()
        {
            @Override
            public void onClick(View v)
            {
                String Primera;
                String Segunda;
                Primera=Entrada.getText().toString();
                Segunda=entrada.getText().toString();
                if (Primera.equals("")||Segunda.equals(""))
                {
                    Salida.setText("Coloque un valor valido en las entradas");
                }
                else
                {
                    Double num1=Double.parseDouble(Primera);
                    Double num2=Double.parseDouble(Segunda);
                    Double num3= num2-num1;
                    final String b=String.valueOf(num3);
                    Salida.setText(b);
                }
            }
        });
        Multiplicacion.setOnClickListener(new View.OnClickListener()
        {
            @Override
            public void onClick(View v)
            {
                String Primera;
                String Segunda;
                Primera=Entrada.getText().toString();
                Segunda=entrada.getText().toString();
                if (Primera.equals("")||Segunda.equals(""))
                {
                    Salida.setText("Coloque un valor valido en las entradas");
                }
                else
                {
                    Double num1=Double.parseDouble(Primera);
                    Double num2=Double.parseDouble(Segunda);
                    Double num3=num2*num1;
                    final String b=String.valueOf(num3);
                    Salida.setText(b);
                }
            }
        });
        Division.setOnClickListener(new View.OnClickListener()
        {
            @Override
            public void onClick(View v)
            {
                String Primera;
                String Segunda;
                Primera=Entrada.getText().toString();
                Segunda=entrada.getText().toString();
                if (Primera.equals("")||Segunda.equals(""))
                {
                    Salida.setText("Coloque un valor valido en las entradas");
                }
                else
                {
                    Double num1=Double.parseDouble(Primera);
                    Double num2=Double.parseDouble(Segunda);
                    Double num3= num2/num1;
                    final String b=String.valueOf(num3);
                    Salida.setText(b);
                }
            }
        });
        Potenciacion.setOnClickListener(new View.OnClickListener()
        {
            @Override
            public void onClick(View v)
            {
                String Primera;
                String Segunda;
                Primera=Entrada.getText().toString();
                Segunda=entrada.getText().toString();
                if (Primera.equals("")||Segunda.equals(""))
                {
                    Salida.setText("Coloque un valor valido en las entradas");
                }
                else
                {
                    Double num1=Double.parseDouble(Primera);
                    Double num2=Double.parseDouble(Segunda);
                    Double num3= Math.pow(num2,num1);
                    final String b=String.valueOf(num3);
                    Salida.setText(b);
                }
            }
        });
        Radicacion.setOnClickListener(new View.OnClickListener()
        {
            @Override
            public void onClick(View v)
            {
                String Primera;
                String Segunda;
                Primera=Entrada.getText().toString();
                Segunda=entrada.getText().toString();
                if (Primera.equals("")||Segunda.equals(""))
                {
                    Salida.setText("Coloque un valor valido en las entradas");
                }
                else
                {
                    Double num1=Double.parseDouble(Primera);
                    Double num2=Double.parseDouble(Segunda);
                    Double num3= Math.pow(num2,(1/num1));
                    final String b=String.valueOf(num3);
                    Salida.setText(b);
                }
            }
        });
        Exit.setOnClickListener(new View.OnClickListener()
        {
            @Override
            public void onClick(View v)
            {
                System.exit(0);
            }
        });

    }
}
